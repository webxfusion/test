<?php

namespace App\Controller;

use App\Repository\PackagerCacheRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Component\BinPackager\Request as PackageRequest;

class ApiController extends AbstractController
{
    private $packagerCacheRepository;

    public function __construct(PackagerCacheRepository $packagerCacheRepository)
    {
        $this->packagerCacheRepository = $packagerCacheRepository;
    }


    /**
     * @Route("/", name="api")
     */
    public function index(Request $request): Response
    {
        $data = json_decode($request->getContent(),true);
        $r = (new PackageRequest($this->getParameter('apiUrl'),$this->getParameter('username'),$this->getParameter('apiKey'),$this->getParameter('bins')))
            ->setItems($data);
        if(!($response = $this->packagerCacheRepository->findOneBy(['hash'=>$r->getCacheHash()])->getResponseData())){
            $response = $this->json($r->getResponse());
            $this->packagerCacheRepository->add($r->getCacheHash(),$response);
        }

        $a=$this->getParameter('test');


        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/ApiController.php',
        ]);
    }
}
