<?php

namespace App\Entity;

use App\Repository\PackagerCacheRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PackagerCacheRepository::class)
 */
class PackagerCache
{
    /**
     * @ORM\Column(name="hash", type="string", length=40, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $hash;

    /**
     * @ORM\Column(name="response_data", type="text", length=65535)
     */
    private $responseData;

    public function getHash(): ?string
    {
        return $this->hash;
    }

    public function setHash(string $hash): self
    {
        $this->hash = $hash;

        return $this;
    }

    public function getResponseData(): ?string
    {
        return $this->responseData;
    }

    public function setResponseData(string $responseData)
    {
        $this->responseData = $responseData;
        return $this;
    }

}
