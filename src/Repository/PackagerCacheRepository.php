<?php

namespace App\Repository;

use App\Component\BinPackager\Request;
use App\Entity\PackagerCache;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method PackagerCache|null find($id, $lockMode = null, $lockVersion = null)
 * @method PackagerCache|null findOneBy(array $criteria, array $orderBy = null)
 * @method PackagerCache[]    findAll()
 * @method PackagerCache[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PackagerCacheRepository extends ServiceEntityRepository
{

    private $manager;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, PackagerCache::class);
        $this->manager = $manager;
    }

    public function add(string $hash,string $responseData)
    {
        $packageCache = new PackagerCache();
        $packageCache->setHash($hash);
        $packageCache->setResponseData($responseData);
        $this->manager->persist($packageCache);
        $this->manager->flush();
    }
}
