<?php


namespace App\Component\BinPackager;


use http\Exception\RuntimeException;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class Request
{

    private $data;

    private $username;

    private $apiKey;

    private $apiUrl;

    private $bins;

    private $client;

    public function __construct(string $apiUrl, string $username, string $apiKey, array $bins)
    {
        $this->username = $username;
        $this->apiKey = $apiKey;
        $this->apiUrl = $apiUrl;
        $this->bins = $bins;
    }

    public function setItems($items)
    {
        $this->data=[];
        $dataAmount=[];
        array_walk($items,function (&$item) use (&$dataAmount){
            if(!(isset($item['width']) && isset($item['height']) && isset($item['length']) && isset($item['weight']))){
                throw new InvalidRequestException();
            }
            $i = [
                'w' => $item['width'],
                'h' => $item['height'],
                'd' => $item['length'],
                'wg' => $item['weight'],
            ];
            $key = array_search($i,$this->data,true);
            if($key===false){
                $this->data[] = $i;
                $dataAmount[array_key_last($this->data)]=['q'=>1];
            }else{
                $dataAmount[$key]['q']++;
            }
        });
        array_walk($this->data,function (&$item,$key) use ($dataAmount){
            $item = array_merge($item,$dataAmount[$key]);
        });
        sort($this->data);
        return $this;
    }

    public function getCacheHash()
    {
        return sha1(json_encode($this->data));
    }

    protected function getUsername()
    {
        return $this->username;
    }

    protected function getApiKey()
    {
        return $this->apiKey;
    }

    protected function getApiUrl()
    {
        return $this->apiUrl;
    }

    protected function getBins()
    {
        return $this->bins;
    }

    protected function getClient(): HttpClientInterface
    {
        if(isset($this->client)){
            $this->client = HttpClient::create();
        }
        return $this->client;
    }

    private function buildRequest()
    {
        return [
            'username' => $this->getUsername(),
            'api_key' => $this->getApiKey(),
            'bins' => $this->getBins(),
            'items' =>$this->data,
        ];
    }

    public function getResponse()
    {
        $response = $this->getClient()->request('POST',$this->getApiUrl,['body'=> $this->buildRequest()]);
        $decodedPayload = $response->toArray();
        $bins =  $response['response']['bins_packed'];
        array_walk($bins,function (&$item){
           $i = [
               'w'=>$item['bin_data']['w'],
               'h'=>$item['bin_data']['h'],
               'd'=>$item['bin_data']['d'],
               'w'=>$item['bin_data']['weight'],
           ];
           $item = $i;
        });

        if(count($bins)>0){
            sort($bins);
            $response = array_shift($bins);
        }else{
            $response = [];
        }

        return $response;
    }

}